﻿// CodeGear C++Builder
// Copyright (c) 1995, 2018 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'DefaultComputator.pas' rev: 33.00 (Windows)

#ifndef DefaultcomputatorHPP
#define DefaultcomputatorHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>
#include <SysInit.hpp>
#include <System.SysUtils.hpp>
#include <Ort_drom.hpp>
#include <BackendPluginInterface.hpp>
#include <BackendPluginManager.hpp>

//-- user supplied -----------------------------------------------------------

namespace Defaultcomputator
{
//-- forward type declarations -----------------------------------------------
class DELPHICLASS TDefaultComputator;
//-- type declarations -------------------------------------------------------
#pragma pack(push,4)
class PASCALIMPLEMENTATION TDefaultComputator : public Backendplugininterface::TComputator
{
	typedef Backendplugininterface::TComputator inherited;
	
public:
	HIDESBASE double __fastcall Compute(const double Left, const double Right);
public:
	/* TObject.Create */ inline __fastcall TDefaultComputator() : Backendplugininterface::TComputator() { }
	/* TObject.Destroy */ inline __fastcall virtual ~TDefaultComputator() { }
	
};

#pragma pack(pop)

//-- var, const, procedure ---------------------------------------------------
}	/* namespace Defaultcomputator */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_DEFAULTCOMPUTATOR)
using namespace Defaultcomputator;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DefaultcomputatorHPP
