unit DefaultComputator;

interface

uses
  SysUtils,
  ort_drom,
  BackendPluginInterface,
  BackendPluginManager;


type

  TDefaultComputator = class(TComputator)
    public
      function Compute(const Left, Right : Double) : Double;
  end;

implementation

{ TDefaultComputator }

function TDefaultComputator.Compute(const Left, Right: Double): Double;
var
  LeftProjection: Double;
  RightProjection: Double;
begin
  CoordTest(0, 0, Left, Right, 1, LeftProjection, RightProjection);
  Exit(LeftProjection + RightProjection);
end;

initialization
  BackendManager.Add(TDefaultComputator)

finalization
  BackendManager.Remove(TDefaultComputator)

end.
